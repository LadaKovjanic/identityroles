﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Identity.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [Authorize(Roles = "Developer")]
        public ActionResult Developer()
        {
            return View();
        }

        [Authorize(Roles = "Designer")]
        public ActionResult Designer()
        {
            return View();
        }

        [Authorize(Roles = "SuperAdmin")]
        public ActionResult Admin()
        {
            return View();
        }

        [Authorize(Roles = "SuperAdmin, Designer, Developer")]
        public ActionResult AllRoles()
        {
            return View();
        }
    }
}