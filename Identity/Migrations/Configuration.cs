namespace Identity.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Identity.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Identity.Models.ApplicationDbContext";
        }

        protected override void Seed(Identity.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            AddUsers(context);
        }

        void AddUsers(Identity.Models.ApplicationDbContext context)
        {
            if (!context.Users.Any())
            {
                var um = new UserManager<ApplicationUser>(
                       new UserStore<ApplicationUser>(context));

                var rm = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
      //Superadmin  
                var role = rm.FindByName("SuperAdmin");
                if (role == null)
                {                    
                    var roleSuperAdmin = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole("SuperAdmin");
                    rm.Create(roleSuperAdmin);
                }

       // Developer  
                 role = rm.FindByName("Developer");
                if (role == null)
                {
                    // first we create Admin rool   
                    var roleDeveloper = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole("Developer ");
                    rm.Create(roleDeveloper);
                }
        // Designer 
                 role = rm.FindByName("Designer");
                if (role == null)
                {
                    // first we create Admin rool   
                    var roleDesigner = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole("Designer");
                    rm.Create(roleDesigner);
                }
               

  //SuperAdmin
                var userAdmin = um.FindByName("mario.mucalo@intellegens.hr");
                if (userAdmin == null)
                {

                    //create admin user
                    var user = new ApplicationUser()
                    {
                        UserName = "mario.mucalo@intellegens.hr",
                        Email = "mario.mucalo@intellegens.hr",
                        PhoneNumber = "00385989517392"
                    };

                    um.Create(user, "!!Intellegens123");
                    um.SetLockoutEnabled(user.Id, false);
                    um.AddToRole(user.Id, "SuperAdmin");
                    context.SaveChanges();
                }

         // Developer  

                userAdmin = um.FindByName("lada.kovjanic@gmail.com");
                if (userAdmin == null)
                {

                    //create  user
                    var user = new ApplicationUser()
                    {
                        UserName = "lada.kovjanic@gmail.com",
                        Email = "lada.kovjanic@gmail.com",
                        PhoneNumber = "00385955256259"
                    };

                    um.Create(user, "!!Intellegens456");
                    um.SetLockoutEnabled(user.Id, false);
                    um.AddToRole(user.Id, "Developer");
                    context.SaveChanges();
                }

         // Designer  

                userAdmin = um.FindByName("lkovjanic@tvz.hr");
                if (userAdmin == null)
                {
                    //create  user
                    var user = new ApplicationUser()
                    {
                        UserName = "lkovjanic@tvz.hr",
                        Email = "lkovjanic@tvz.hr",
                        PhoneNumber = "00385955256259"
                    };

                    um.Create(user, "!!Intellegens789");
                    um.SetLockoutEnabled(user.Id, false);
                    um.AddToRole(user.Id, "Designer");
                    context.SaveChanges();
                }
            }
        }

    }
}
